all:
	$(MAKE) clean
	$(MAKE) run-tests

clean:
	rm -rf build
	rm -rf Products
	rm -rf xtc-submit-*

  # Legacy
	rm -rf *.ipa
	rm -rf *.app.dSYM
	rm -rf *.app
	rm -rf xtc-staging


# Builds an app from the CalSmoke-cal target.
#
# This app links the calabash.framework during the build.
run-tests:
	scripts/make/app-cal.sh
	sh scripts/test/run-tests.sh

